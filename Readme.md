# Simple web app with C++ backend

## What to do
Develop a C++ web server that provides the following endpoints:

* /ui provides the content of the web folder
* /author returns a string containing the name of the author of the webserver
* /add_item takes one parameter (item) that has to be added to a collections
* /list_items returns a space separated, sorted list of items previously added
* /count_unique returns the amount of unique items in the list

## FAQ

* I don't see anything when opening localhost:80 in my browser
  * Make sure you use the complete URL, like localhost/ui and that the path to your static fine folder is correct