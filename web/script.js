
var App = new Vue({
    el: '#app',
    data: {
        items : [],
        item_to_add : '',
        author :'??',
        unique: 0
    },

    created : function(){
        this.get_items();

        obj = this;
        axios.get('author')
            .then(function (response) {
                obj.author = response.data;
            })
            .catch(function (error) {
                console.log(error);
                obj.author = 'Error';
            });
    },

    methods : {
        get_items : function () {
            obj = this;
            axios.get('list_items')
                .then(function (response) {
                    obj.items = response.data.trim().split(' ');
                })
                .catch(function (error) {
                    console.log(error);
                    obj.items = [];
                });

            axios.get('count_unique')
                .then(function (response) {
                    obj.unique = response.data;
                })
                .catch(function (error) {
                    console.log(error);
                    obj.unique = -1;
                });
        },

        add_item : function () {
            item = this.item_to_add;
            this.item_to_add = '';
            obj = this;
            axios.get('add_item', {
                    params: {
                        'item' : item
                    }
                })
                .then(function (response) {
                    obj.get_items();
                })
                .catch(function (error) {
                    console.log(error);
                    obj.get_items();
                });
        }
    }
})